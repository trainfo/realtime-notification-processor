/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Chao
 */
public class IsEntry {
    
    boolean isEntry = false;

    public IsEntry(String monitorID) throws SQLException {
        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        //System.out.println("db connected!");
        
        String query = "SELECT direction FROM entryandexit WHERE monitor_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, monitorID);
        rs = stmt.executeQuery();
        while (rs.next()) {
            isEntry = true;
            System.out.println("---" + monitorID + " is an entry/exit");
        }
    }
}
