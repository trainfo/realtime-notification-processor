/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class LastMonitor {
    String lstMonID;
    
    public LastMonitor(String crtMonID, Timestamp crtT, Monitor m) throws SQLException {
        this.lstMonID = null;
        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        String query = "SELECT last_scan, ls_time, cs_time FROM section_activity WHERE current_scan = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, crtMonID);
        rs = stmt.executeQuery();
        
        
        while (rs.next()) {
            ResultSet rs2 = null;
            String query2 = "SELECT min_threshold, max_threshold FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
            stmt = con.prepareStatement(query2);
            stmt.setString(1, rs.getString("last_scan"));
            stmt.setString(2, crtMonID);
            rs2 = stmt.executeQuery();
            long mTDiff = (crtT.getTime() - rs.getTimestamp("cs_time").getTime())/1000;
            if(mTDiff > m.min_threshold && mTDiff < m.max_threshold) {
                long sTDiff = (rs.getTimestamp("cs_time").getTime() - rs.getTimestamp("ls_time").getTime())/1000;
                while(rs2.next()){
                    if(sTDiff<rs2.getInt("max_threshold") && sTDiff>rs2.getInt("min_threshold"))
                    {
                        lstMonID = rs.getString("last_scan");
                        break;
                    }
                }
            }
        }
        System.out.println("---last monitor ID is "+lstMonID);
    }
    
    
    
}
