/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class SectionThread implements Runnable{
    protected Monitor monitor_orig;
    protected Monitor monitor_dest;
    protected long minTh;
    protected long maxTh;
    protected List<Scan> scanList;
    //save KML to file
    Transformer transformer;
    StreamResult result;
    DOMSource source;
    //set up xml parser
    DOMParser parser;
    
    Document doc;
    NodeList nl; 
    //get length of node list - how many styleUrl node there are in the list
    int len;
    Element e;
    
    String lineName;
    String lineName2;

    public SectionThread(Monitor monitor_orig, Monitor monitor_dest, long minTh, long maxTh) throws TransformerConfigurationException, SAXException, IOException {
        this.monitor_orig = monitor_orig;
        this.monitor_dest = monitor_dest;
        this.minTh = minTh;
        this.maxTh = maxTh;
        this.lineName = monitor_dest.getID()+"-"+monitor_orig.getID();
        this.lineName2 = monitor_orig.getID()+"-"+monitor_dest.getID();
        this.transformer = TransformerFactory.newInstance().newTransformer();
        this.transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        
    }
     
    public void run() {
        try {
                scanList = new ArrayList<>();
                Thread.sleep(maxTh * 1000);
                System.out.println("------section of " + monitor_orig.ID + " and " + monitor_dest.ID + " max threshold exceeded");
                if(!monitor_dest.getScanList().isEmpty()) {
                    for(int j = monitor_dest.getScanList().size()-1; j >= 0; j--)
                    {
                        long scanT = monitor_dest.getScanList().get(j).getTimestamp().getTime();
                        //get current time
                        Calendar calendar = Calendar.getInstance();
                        long cT = calendar.getTime().getTime();
                        long diff = (cT - scanT) / 1000;
                        long range = maxTh - minTh;
                        //if scans from adjacent monitors fall into the timeframe
                        //the direction can be determined to be from the adjacent monitor where the scan was found to 
                        //the current monitor
                        //so that the adjacent monitor where the scan was found will not be warned
                        //only the adjacent monitor where no scan meet the timeframe is found will be warned
                        if(diff <= range)
                        {  
                            System.out.println("------warning has been cleared within threshold");
                            scanList.add(monitor_dest.getScanList().get(j));
                            break;
                        }
                    }
                }

                if(scanList.isEmpty()) {  
                    
                    this.parser = new DOMParser();
                    parser.parse("EmersonSensors - Static Notification.kml");
                    this.doc = parser.getDocument();
                    this.nl = doc.getElementsByTagName("styleUrl");
                    this.len = nl.getLength();

                    IsAsset ia = new IsAsset(monitor_orig.ID, monitor_dest.ID);
                    if(ia.isAsset == true) {
                        System.out.println("------st exceeded and warning cleared");
                        System.out.println("------there is asset between " + monitor_dest.ID + " and " + monitor_orig.ID);
                        for (int i = 0; i < len; i++) {
                            e = (Element) nl.item(i);
                            String styleUrlID = e.getAttribute("id");
                            if (styleUrlID.equals(monitor_dest.getID())) {
                                e.setTextContent("#clear");
                                System.out.println(styleUrlID + " --> #cleared");
                            }
                        }
                    }else {
                        System.out.println("------st exceeded and warning cleared");
                        System.out.println("------notify " + monitor_dest.ID );
                        for (int i = 0; i < len; i++) {
                            e = (Element) nl.item(i);
                            String styleUrlID = e.getAttribute("id");
                            if (styleUrlID.equals(monitor_dest.getID())) {
                                e.setTextContent("#clear");
                                System.out.println(styleUrlID + " --> #cleared");    
                            }
                        }
                    }
                    
                    //change section line style to clear line
                    for (int j = 0; j < len; j++) {          
                        e = (Element) nl.item(j);
                        if(lineName.equals(e.getAttribute("id"))||lineName2.equals(e.getAttribute("id"))) {
                            e.setTextContent("#clearLine");
                        }
                    }
                    
                    //save KML to file         
                    this.result = new StreamResult(new File("EmersonSensors - Static Notification.kml"));
                    this.source = new DOMSource(doc);
                    transformer.transform(source, result);
                }

        } catch (InterruptedException | SQLException | SAXException | IOException ex) {
            System.out.println("------st has been interrupted normally");

        } catch (TransformerException ex) {
            Logger.getLogger(SectionThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
