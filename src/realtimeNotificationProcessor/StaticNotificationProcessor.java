/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import java.io.*;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class StaticNotificationProcessor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, SAXException, TransformerException {
        // TODO code application logic here
        new ProcessorServerThread().start();
        //new UpdateKML("#blocked", "CA8323300811005");
    }
    
}
