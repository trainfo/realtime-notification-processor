/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import java.sql.Timestamp;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class Scan {

    protected String monitorID;

    protected Timestamp timestamp;

    protected int digi_in;
    

    public Scan(String monitorID, Timestamp timestamp, int digi_in) {
        this.monitorID = monitorID;
        this.timestamp = timestamp;
        this.digi_in = digi_in;
    }

    
    
    /**
     * Get the value of digi_in
     *
     * @return the value of digi_in
     */
    public int getDigi_in() {
        return digi_in;
    }

    /**
     * Set the value of digi_in
     *
     * @param digi_in new value of digi_in
     */
    public void setDigi_in(int digi_in) {
        this.digi_in = digi_in;
    }

    /**
     * Get the value of Timestamp
     *
     * @return the value of Timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Set the value of Timestamp
     *
     * @param Timestamp new value of Timestamp
     */
    public void setTimestamp(Timestamp Timestamp) {
        this.timestamp = Timestamp;
    }

    /**
     * Get the value of monitorID
     *
     * @return the value of monitorID
     */
    public String getMonitorID() {
        return monitorID;
    }

    /**
     * Set the value of monitorID
     *
     * @param monitorID new value of monitorID
     */
    public void setMonitorID(String monitorID) {
        this.monitorID = monitorID;
    }

}
