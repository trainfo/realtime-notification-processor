/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class Monitor {

    protected String ID;
    protected List<Scan> scanList;
    protected String status;
    protected int min_threshold;
    protected int max_threshold;
    protected int expectArrivingTrain;
    protected List<Thread> monThreadList;
    protected Map<String, Thread> secThreadList;
    
    public Monitor() {
    }

    public Monitor(String ID, List<Scan> scanList, String status, int minT, int maxT) {
        this.ID = ID;
        this.scanList = scanList;
        this.status = status;
        this.min_threshold = minT;
        this.max_threshold = maxT;
        this.monThreadList = new ArrayList<>();
        this.secThreadList = new HashMap<>();
    }

    public int getExpectTimesofTrain() {
        return expectArrivingTrain;
    }

    public void setExpectTimesofTrain(int expectTimesofTrain) {
        this.expectArrivingTrain = expectArrivingTrain;
    }
    
    public int getMin_threshold() {
        return min_threshold;
    }

    public void setMin_threshold(int min_threshold) {
        this.min_threshold = min_threshold;
    }

    public int getMax_threshold() {
        return max_threshold;
    }

    public void setMax_threshold(int max_threshold) {
        this.max_threshold = max_threshold;
    }
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public List<Scan> getScanList() {
        return scanList;
    }

    public void setScanList(List<Scan> scanList) {
        this.scanList = scanList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
