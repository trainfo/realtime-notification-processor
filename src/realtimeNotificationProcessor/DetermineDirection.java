
package realtimeNotificationProcessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author Chao at Morr Information Systems of Morr Transportation Consulting
 */
public class DetermineDirection {

    List<String> nxtCrsList = new ArrayList<>();
    List<Scan> prevScanList = new ArrayList<>();
    Map<String, Thread> prevSecThList = new HashMap<>();
    
    public DetermineDirection(String monitorID, Timestamp currentT, Map<String, Monitor> monMap) throws SQLException, TransformerConfigurationException, TransformerException, SAXException, IOException {
        List<String> mIDList = new ArrayList<>();
        
        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        con.setAutoCommit(false);
        
        //find possible next end points 
        System.out.println("---determine section end points....");
        //query for finding possible end points
        String query = "SELECT monitor2_id FROM section WHERE monitor1_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, monitorID);
        rs = stmt.executeQuery();
        while (rs.next()) {
            mIDList.add(rs.getString("monitor2_id"));
        }

        outerForLoop:
        for(int j = 0; j < mIDList.size(); j++)
        {
            System.out.println("---possible next monitor is " + mIDList.get(j));
            long minTh = 0;
            long maxTh = 0;
            //for each possible end monitor
            //get thresholds for possible previous scans
            String query2 = "SELECT min_threshold, max_threshold FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
            stmt = con.prepareStatement(query2);
            stmt.setString(1, mIDList.get(j));
            stmt.setString(2, monitorID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                //get possible end points
                minTh = rs.getInt("min_threshold");
                maxTh = rs.getInt("max_threshold");
            }

            System.out.println("---min and max thresholds for  " + mIDList.get(j) + " to current monitor is " + minTh + " & " +maxTh);

            //if possible end points has scans in scan list
            if(!monMap.get(mIDList.get(j)).getScanList().isEmpty()) {
                //get scan list of possbile next monitors and look for previous scans
                for(int m = monMap.get(mIDList.get(j)).getScanList().size()-1; m >=0; m--)
                {
                    if(monMap.get(mIDList.get(j)).getScanList().get(m).digi_in == 1 ) {
                        continue;
                    }
                    //loop starts from the last scan inserted into the list
                    long scanT = monMap.get(mIDList.get(j)).getScanList().get(m).getTimestamp().getTime();

                    long diff = (currentT.getTime() - scanT) / 1000;
                    //if scans from adjacent monitors fall into the timeframe
                    //the direction can be determined to be from the adjacent monitor where the scan was found to 
                    //the current monitor
                    //so that the adjacent monitor where the scan was found will not be warned
                    //only the adjacent monitor where no scan meet the timeframe is found will be warned
                    if(diff < maxTh && diff > minTh)
                    {  
                        //store all scans with timestamp falls into the min and max threshold range
                        prevScanList.add(monMap.get(mIDList.get(j)).getScanList().get(m));         

                        PreparedStatement stmt2 = null;
                        //query
                        String query3 = "INSERT INTO section_activity (current_scan, cs_time, last_scan, ls_time) VALUES (?, ?, ?, ?)";
                        stmt2 = con.prepareStatement(query3);
                        //feed value being inserted
                        stmt2.setString(1, monitorID);
                        stmt2.setTimestamp(2, currentT);
                        stmt2.setString(3, mIDList.get(j));
                        stmt2.setTimestamp(4, monMap.get(mIDList.get(j)).getScanList().get(m).timestamp);
                        stmt2.executeUpdate();
                        //commit the insertion
                        con.commit();
                        System.out.println("---last scan time at this monitor is " + monMap.get(mIDList.get(j)).getScanList().get(m).getTimestamp());
                        System.out.println("---The timestamp is within the threshold so this monitor will not be warned");
                        if(stmt2 != null) stmt2.close();
                        
                        prevSecThList = monMap.get(mIDList.get(j)).secThreadList;
                        continue outerForLoop;
                    }
                }
            }
            
            nxtCrsList.add(mIDList.get(j));
            //to add code here for auditing next expected crossing into DB
            //...
            
            //monMap.get(mIDList.get(j)).expectArrivingTrain++;
            System.out.println("---next crossing to be warned is " + mIDList.get(j));
            //System.out.println("---the number of trains expected at next crossing is " + monMap.get(mIDList.get(j)).expectArrivingTrain);
        }
        
        
        
        if(rs != null) rs.close();
        if(stmt != null) stmt.close();
        con.close();
    }
    
}
