/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class MonitorThread extends Thread {
    
    protected Monitor monitor;
    protected Scan scan;

    public MonitorThread() {
    }
    
    public MonitorThread(Monitor monitor, Scan scan) {
        this.monitor = monitor;
        this.scan = scan;
    }
    
    public void run() {
        try {
/*
            Thread t = new Thread(new Threshold());
            t.start();
            t.join();*/
            Thread.sleep(monitor.max_threshold * 1000);
            System.out.println("------monitor threshold exceeded");
            
            /*
            //exceed the threshold and clear the monitor blockage
            //set up xml parser
            DOMParser parser = new DOMParser();
            parser.parse("EmersonSensors.kml");
            Document doc = parser.getDocument();
            NodeList nl = doc.getElementsByTagName("styleUrl");
            //get length of node list - how many styleUrl node there are in the list
            int len;
            len = nl.getLength();
            Element e;
            
            for (int i = 0; i < len; i++) {
                e = (Element) nl.item(i);
                String styleUrlID = e.getAttribute("id");
                if (styleUrlID.equals(monitor.getID())) {
                    e.setTextContent("#clear");
                    System.out.println(styleUrlID + " --> #cleared due to scan threshold exceeded");
                }
            }
            */
          
            monitor.monThreadList.clear();
            
            /*
            //save KML to file
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,
                    "yes");
            StreamResult result = new StreamResult(new File("EmersonSensors.kml"));
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
                    */
        } catch (InterruptedException ex) {
            System.out.println("------mt has been interrupted");
            monitor.monThreadList.clear();
        }
    }
}
