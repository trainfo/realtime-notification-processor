/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Chao
 */
public class SegmentThread implements Runnable {
    int maxTh;
    String agcrossing_id;
    DOMParser parser;
    Document doc;
    NodeList nl;
    //get length of node list - how many styleUrl node there are in the list
    int len;
    Node n;
    Element e;
    
    public SegmentThread(int agcrossing_id, int maxTh) throws SAXException, IOException {
        this.maxTh = maxTh;
        this.agcrossing_id = Integer.toString(agcrossing_id);
        
    }

    @Override
    public void run() {
        try {
            Thread.sleep(this.maxTh * 1000);
            System.out.println("------segment thread ended");
            //set up xml parser
            this.parser = new DOMParser();
            parser.parse("EmersonSensors.kml");
            this.doc = parser.getDocument();
            this.nl = doc.getElementsByTagName("styleUrl");
            len = nl.getLength();
            for(int m = 0; m < len; m++){
                //for each styleUrl with monitor ID
                e = (Element) nl.item(m);
                //for each next crossing monitor ID
                String styleUrlID = e.getAttribute("id");
                //compare the two IDs
                if(styleUrlID.equals(this.agcrossing_id)){
                    if ("#agcaution".equals(e.getTextContent())) {
                        e.setTextContent("#agclear");
                        System.out.println(styleUrlID + " --> #agclear");
                        e.getPreviousSibling().getPreviousSibling().setTextContent("");
                    }
                }
            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            //initialize StreamResult with File object to save to file
            StreamResult result = new StreamResult(new File("EmersonSensors.kml"));
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
        } catch (InterruptedException | TransformerException | SAXException | IOException ex) {
            Logger.getLogger(SegmentThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
