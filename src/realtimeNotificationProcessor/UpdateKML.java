package realtimeNotificationProcessor;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Chao at MORR Information Systems
 */
public class UpdateKML {

    DOMParser parser;
    Document doc;
    //get length of node list - how many styleUrl node there are in the list
    int len;
    String nn;
    NodeList nl;
    Node n;
    Element e;

    public UpdateKML() {
    }
    
    
    public UpdateKML(String status, String monitorID) throws IOException, SAXException, TransformerConfigurationException, TransformerException, SQLException {
        //set up xml parser
        this.parser = new DOMParser();
        this.parser.parse("RealTimeNotificationSystem.kml");
        this.doc = this.parser.getDocument();
        this.nl = this.doc.getElementsByTagName("styleUrl");
        this.len = this.nl.getLength();
        List<String> mIDList = new ArrayList<>();
        List<String> nxtCrsList = new ArrayList<>();
        
        //iterate all styleUrl node
        for (int i = 0; i < this.len; i++) {
            this.e = (Element) this.nl.item(i);
            //nnm = e.getAttributes();
            //n = nl.item(i);
            //System.out.print(e.getTextContent() + " ");
            String styleUrlID = this.e.getAttribute("id");
            if (styleUrlID.equals(monitorID)) {
                //System.out.println("styUrl id and monitor id equals");
                //update status for corresponding node no matter what the status is
                this.e.setTextContent(status);
                //e.getPreviousSibling().getPreviousSibling().setTextContent("");
            }
        }
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT,
                "yes");

        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("RealTimeNotificationSystem.kml"));
        DOMSource source = new DOMSource(doc);

        transformer.transform(source, result);

    }
    
    public UpdateKML(String monitorID) throws SAXException, IOException, TransformerConfigurationException, TransformerException {
        
        //set up xml parser
        this.parser = new DOMParser();
        this.parser.parse("RealTimeNotificationSystem.kml");
        this.doc = this.parser.getDocument();
        this.nl = this.doc.getElementsByTagName("styleUrl"); 
        this.len = this.nl.getLength();
        
        //iterate all styleUrl node
        for (int i = 0; i < this.len; i++) {
            this.e = (Element) this.nl.item(i);
            //nnm = e.getAttributes();
            //n = nl.item(i);
            //System.out.print(e.getTextContent() + " ");
            String styleUrlID = this.e.getAttribute("id");
            if (styleUrlID.equals(monitorID)) {
                if ("#caution".equals(e.getTextContent())) {
                //System.out.println("styUrl id and monitor id equals");
                //update status for corresponding node no matter what the status is
                this.e.setTextContent("#clear");
                e.getPreviousSibling().getPreviousSibling().setTextContent("");
                System.out.println(styleUrlID + " --> #cleared");
                }else {
                    System.out.println("---"+ styleUrlID + " found in KML but status is not #caution");
                }
            }
        }
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT,
                "yes");

        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("RealTimeNotificationSystem.kml"));
        DOMSource source = new DOMSource(doc);

        transformer.transform(source, result);
    }
    
    public void clearWarning(String monitorID) throws SAXException, IOException, TransformerException {
        //set up xml parser
        this.parser = new DOMParser();
        this.parser.parse("RealTimeNotificationSystem.kml");
        this.doc = this.parser.getDocument();
        this.nl = this.doc.getElementsByTagName("styleUrl"); 
        this.len = this.nl.getLength();
        
        //iterate all styleUrl node
        for (int i = 0; i < this.len; i++) {
            this.e = (Element) this.nl.item(i);
            String styleUrlID = this.e.getAttribute("id");
            if (styleUrlID.equals(monitorID)) {
                //System.out.println("styUrl id and monitor id equals");
                //update status for corresponding node no matter what the status is
                this.e.setTextContent("#clear");
                System.out.println(styleUrlID + " --> #cleared");
                e.getPreviousSibling().getPreviousSibling().setTextContent(styleUrlID);
            }
        }
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("RealTimeNotificationSystem.kml"));
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
    }
    
    public void clearLine(String lineName, String lineName2) throws SAXException, IOException, TransformerException {
        //set up xml parser
        this.parser = new DOMParser();
        this.parser.parse("RealTimeNotificationSystem.kml");
        this.doc = this.parser.getDocument();
        this.nl = this.doc.getElementsByTagName("styleUrl");
        this.len = this.nl.getLength();
        for(int j = 0; j < len; j++){
            e = (Element) nl.item(j);   
            if(lineName.equals(e.getAttribute("id")) || lineName2.equals(e.getAttribute("id"))) {
                e.setTextContent("#clearLine");
            }
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("RealTimeNotificationSystem.kml"));
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
    }
    
    public void clearLinetoLastMon(String lineName, String lineName2, String lstMon) throws SAXException, IOException, TransformerException {
        //set up xml parser
        this.parser = new DOMParser();
        this.parser.parse("RealTimeNotificationSystem.kml");
        this.doc = this.parser.getDocument();
        this.nl = this.doc.getElementsByTagName("styleUrl");
        this.len = this.nl.getLength();
        for(int j = 0; j < len; j++){
            e = (Element) nl.item(j);
            if(lineName.equals(e.getAttribute("id")) || lineName2.equals(e.getAttribute("id"))) {
                e.setTextContent("#clearLine");
            }
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("RealTimeNotificationSystem.kml"));
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
    }

}
