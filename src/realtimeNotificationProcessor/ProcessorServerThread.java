package realtimeNotificationProcessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.net.InetSocketAddress;

/**
 *
 * @author Chao at MORR Information Systems
 */
public class ProcessorServerThread extends Thread {

    protected DatagramSocket socket = null;
    protected BufferedReader in = null;
    protected boolean moreScans = true;
    protected ArrayList monitorList;

    /**
     * This is the default constructor for this class without parameters
     *
     * @throws IOException
     */
    public ProcessorServerThread() throws IOException {
        this("ProcessorServerThread");
    }

    /**
     * This is a overload constructor of this class
     *
     * @param name the name of the predictive processor thread 
     * @throws java.io.IOException 
     */
    public ProcessorServerThread(String name) throws IOException {
        //set the name of the thread
        super(name);
        //set up udp socket at port 4445
        socket = new DatagramSocket(null);
        InetSocketAddress address = new InetSocketAddress("172.31.43.44", 4445);
        socket.bind(address);
    }

    /**
     * This run method starts the thread for predictive processor
     */
    public void run() {

        //define and init status var for updating monitor node status in KML
        String status = "#clear";
        //initiate three scan lists for three monitors
        List<Scan> scanListCotton = new ArrayList<>();
        List<Scan> scanListMarion = new ArrayList<>();
        List<Scan> scanListSturgeon = new ArrayList<>();
        List<Scan> scanList15101 = new ArrayList<>();
        List<Scan> scanListProvencher = new ArrayList<>();
        List<Scan> scanList15207 = new ArrayList<>();
        List<Scan> scanListMcgillivray = new ArrayList<>();
        List<Scan> scanListBison = new ArrayList<>();
        List<Scan> scanListShaftesbury = new ArrayList<>();
        List<Scan> scanListStAnne = new ArrayList<>();
        List<Scan> scanListWaverley = new ArrayList<>();
        
        //init monitors that represents all monitors that are physically installed in the field
        Monitor mMarion = new Monitor("CA8323300811005", scanListMarion, status, 4, 36);
        Monitor mCottonwood = new Monitor("CA8040402711008", scanListCotton, status, 6, 15);
        Monitor mSturgeon = new Monitor("CA8058401391009", scanListSturgeon, status, 6, 15);
        Monitor m15101 = new Monitor("CA8058402571009", scanList15101, status, 6, 15);
        Monitor mProvencher = new Monitor("CA8058406711009", scanListProvencher, status, 6, 15);
        Monitor m15207 = new Monitor("CA8058409291009", scanList15207, status, 6, 15);
        Monitor mMcgillivray = new Monitor("CA8058410081009", scanListMcgillivray, status, 6, 15);
        Monitor mBison = new Monitor("CA8058411071009", scanListBison, status, 6, 15);
        Monitor mShaftesbury = new Monitor("CA8058411461009", scanListShaftesbury, status, 6, 15);
        Monitor mStAnne = new Monitor("CA8058410141009", scanListStAnne, status, 6, 15);
        Monitor mWaverley = new Monitor("PIE006", scanListWaverley, status, 6, 15);
        
        //init a hash map for mapping monitor name to monitor object
        Map<String, Monitor> monMap = new HashMap<>();
       
        monMap.put("CA8323300811005", mMarion);
        monMap.put("CA8040402711008", mCottonwood);
        monMap.put("CA8058401391009", mSturgeon);
        monMap.put("CA8058402571009", m15101);
        monMap.put("CA8058406711009", mProvencher);
        monMap.put("CA8058409291009", m15207);
        monMap.put("CA8058410081009", mMcgillivray);
        monMap.put("CA8058411071009", mBison);
        monMap.put("CA8058411461009", mShaftesbury);
        monMap.put("CA8058410141009", mStAnne);
        monMap.put("PIE006", mWaverley);
        
        List<String> nxtCrssngList = new ArrayList<>();
        List<Scan> prevScanList = new ArrayList<>();
        
        while (moreScans) {

            PreparedStatement stmt = null;
            try {
                // initiate varibles to store received parameters
                String ip = "9.0.0.0";
                String deviceID = "CA0";
                int diChTo = 9;
                byte[] buf = new byte[256];

                // receive udp request
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                System.out.println("---waiting for data...");
                socket.receive(packet);

                // convert the contents to a string, and display them
                String msg = new String(buf, 0, packet.getLength());
                String submsg;
                submsg = msg.substring(6);
                //System.out.println(packet.getAddress().getHostName() + ": " + msg);
                System.out.println("---raw scan data: " + msg.substring(6));

                //set up XML parser using DOM API
                DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(submsg));
                Document doc = db.parse(is);
                
                //get all nodes named sts
                NodeList stsNodes = doc.getElementsByTagName("sts");

                for (int i = 0; i < stsNodes.getLength(); i++) {

                    //transform each sts node into element
                    Element element = (Element) stsNodes.item(i);
                    String id = element.getAttribute("id");
                    //use switch to associate IDs with corresponding variables
                    switch (id) {
                        case "851":
                            diChTo = Integer.parseInt(element.getTextContent());
                            break;
                        case "301":
                            ip = element.getTextContent();
                            break;
                    }
                }

                //get all nodes named inf
                NodeList infNodes = doc.getElementsByTagName("inf");

                for (int i = 0; i < infNodes.getLength(); i++) {
                    Element element = (Element) infNodes.item(i);
                    String id = element.getAttribute("id");
                    switch (id) {
                        case "25":
                            deviceID = element.getTextContent();
                    }
                }
                
                //get current time
                Calendar calendar = Calendar.getInstance();
                Timestamp currentT = new Timestamp(calendar.getTime().getTime());

                //change status varibale according to digital input value
                //and start the thread on the monitor with the deviceID
                if (diChTo == 0) {
                    status = "#blocked";   
                    System.out.println(deviceID + " --> #blocked @" + currentT);
                } else {
                    status = "#clear";
                    System.out.println(deviceID + " --> #cleared @" + currentT);
                }
                
                 //update KML 
                new UpdateKML(status, deviceID);

                //add scan to a scanlist associated with a specific monitor
                Scan scan = new Scan(deviceID, currentT, diChTo);

                /*
                //set up db connection 
                //ResultSet rs = null;
                MysqlDataSource ds = new MysqlDataSource();
                ds.setServerName("localhost");
                ds.setDatabaseName("trainfo_demo");
                Connection con = ds.getConnection("root", "bcyx10020719");
                //set to not commit automatically after query execution
                con.setAutoCommit(false);
                //query
                String query = "INSERT INTO scan (timestamp, monitor_id, ip, di_change_to) VALUES (?, ?, ?, ?)";
                stmt = con.prepareStatement(query);

                //feed value being inserted
                stmt.setTimestamp(1, currentT);
                stmt.setString(2, deviceID);
                stmt.setString(3, ip);
                stmt.setInt(4, diChTo);
                stmt.executeUpdate();
                
                //commit the insertion
                con.commit();
                con.close();
                */
            } catch (IOException e) {
                moreScans = false;
            } catch (ParserConfigurationException | SAXException | SQLException | TransformerException ex) {
                Logger.getLogger(ProcessorServerThread.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(ProcessorServerThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        socket.close();
    }
}
