/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chao
 */
public class IsAsset {
    boolean isAsset = false;
    String asset = null;
    List<Integer> secIDList = new ArrayList<>();
    Integer section_id = 0;

    public IsAsset(String deviceID) throws SQLException {
        
        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        //System.out.println("db connected!");
        
        String query = "SELECT id FROM section WHERE monitor2_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, deviceID);
        rs = stmt.executeQuery();
        while (rs.next()) {
            secIDList.add(rs.getInt("id"));
        }
        
        if(secIDList.isEmpty())
        {
            System.out.println("------fail to get any section id from DB");
        } else {
            for(int i = 0 ; i < secIDList.size(); i++) {
                String query2 = "SELECT name FROM assets WHERE section_id = ?";
                stmt = con.prepareStatement(query2);
                stmt.setInt(1, secIDList.get(i));
                rs = stmt.executeQuery();
                while (rs.next()) {
                    asset = rs.getString("name");
                    System.out.println("------anomaly because of asset " + asset);
                    isAsset = true;
                }
            }
            if(asset == null) {
                System.out.println("------notify because of no asset");
                isAsset = false;
            }
        }
    }
    
    public IsAsset(String origin, String dest) throws SQLException {
        
        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        //System.out.println("db connected!");
        
        String query = "SELECT id FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, origin);
        stmt.setString(2, dest);
        rs = stmt.executeQuery();
        while (rs.next()) {
            section_id = rs.getInt("id");
        }
        
        if(section_id == 0)
        {
            System.out.println("------fail to get any section id from DB");
        } else {
            String query2 = "SELECT name FROM assets WHERE section_id = ?";
            stmt = con.prepareStatement(query2);
            stmt.setInt(1, section_id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                asset = rs.getString("name");
                //System.out.println("------clear warning because of asset " + asset);
                isAsset = true;
            }
            if(asset == null) {
                //System.out.println("------notify because of no asset and clear warning");
                isAsset = false;
            }
        }
    }
}
