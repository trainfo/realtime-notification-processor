/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class WarnNxtCrssng {

    public WarnNxtCrssng(String monitorID, Map<String, Monitor> monMap, List<String> nxtCrsList) throws SAXException, IOException, SQLException, TransformerConfigurationException, TransformerException {
        //set up xml parser
        DOMParser parser = new DOMParser();
        parser.parse("EmersonSensors.kml");
        Document doc = parser.getDocument();
        NodeList nl = doc.getElementsByTagName("styleUrl");
        NodeList nl2 = doc.getElementsByTagName("Placemark");
        
        //get length of node list - how many styleUrl node there are in the list
        int len;
        len = nl.getLength();
        Node n;
        Element e;
        Element e2;

        //set up db connection 
        ResultSet rs = null;
        PreparedStatement stmt = null;
        MysqlDataSource ds = new MysqlDataSource();
        ds.setServerName("localhost");
        ds.setDatabaseName("trainfo_demo");
        Connection con = ds.getConnection("root", "bcyx10020719");
        
        //warn next crossings
        for ( int k = 0; k < nxtCrsList.size(); k++){
            
            for(int i = 0; i < len; i++){
                //for each styleUrl with monitor ID
                e = (Element) nl.item(i);
                //for each next crossing monitor ID
                String styleUrlID = e.getAttribute("id");
                //compare the two IDs
                if(styleUrlID.equals(nxtCrsList.get(k))){
                    if ("#clear".equals(e.getTextContent())) {
                        e.setTextContent("#caution");
                        System.out.println(styleUrlID + " --> #caution");
                      
                        //init db agent to help get agcrossing names and the corresponding seg threshold
                        DBAgent dba = new DBAgent(rs, stmt, con, ds);
                        List<Integer> segCrossing = new ArrayList<>();
                        
                        segCrossing = dba.getSegmentCrossing(monitorID, styleUrlID);
                        List<Integer> segThreshold = new ArrayList<>();
                        for(int j = 0 ; j < segCrossing.size(); j++) {
                            segThreshold.clear();
                            segThreshold = dba.getSegmentThreshold(monitorID, segCrossing.get(j));
                            if(!segThreshold.isEmpty()) {
                                for(int m = 0; m < len; m++){
                                    //for each styleUrl with monitor ID
                                    e2 = (Element) nl.item(m);
                                    //for each next crossing monitor ID
                                    String styleUrlID2 = e2.getAttribute("id");
                                    //compare the two IDs
                                    if(styleUrlID2.equals(segCrossing.get(j).toString())){
                                        if ("#agclear".equals(e2.getTextContent())) {
                                            e2.setTextContent("#agcaution");
                                            System.out.println(styleUrlID2 + " --> #agcaution");
                                            //update placemark name to include min and max th
                                            Calendar now = Calendar.getInstance();
                                            now.add(Calendar.MINUTE, (int)segThreshold.get(0)/60);
                                            now.add(Calendar.SECOND, (int)segThreshold.get(0)%60);

                                            e2.getPreviousSibling().getPreviousSibling().setTextContent(now.get(Calendar.HOUR_OF_DAY)+":"+now.get(Calendar.MINUTE)+":"+now.get(Calendar.SECOND));
                                        
                                            //start section thread
                                            Thread st = new Thread(new SegmentThread(segCrossing.get(j), segThreshold.get(1)-segThreshold.get(0)));
                                            System.out.println("------segment thread" + st.getName() + "started");
                                            st.start();
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        //get section thresholds of current monitor and next crossings
                        long minTh = 0;
                        long maxTh = 0;
                        String query3 = "SELECT min_threshold, max_threshold FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
                        stmt = con.prepareStatement(query3);
                        stmt.setString(1, monitorID);
                        stmt.setString(2, styleUrlID);
                        rs = stmt.executeQuery();
                        while (rs.next()) {
                            //get possible end points
                            minTh = rs.getInt("min_threshold");
                            maxTh = rs.getInt("max_threshold");
                        }
                        
                        //update placemark name to include min and max th
                        Calendar now = Calendar.getInstance();
                        now.add(Calendar.MINUTE, (int)minTh/60);
                        now.add(Calendar.SECOND, (int)minTh%60);
                        
                        e.getPreviousSibling().getPreviousSibling().setTextContent(now.get(Calendar.HOUR_OF_DAY)+":"+now.get(Calendar.MINUTE)+":"+now.get(Calendar.SECOND));
                        
                        System.out.println("---section threshold starts");
                        
                        //start section thread
                        Thread st = new Thread(new SectionThread(monMap.get(monitorID), monMap.get(nxtCrsList.get(k)), minTh, maxTh));
                        st.start();
                        //add section thread to the monitor that initiates that
                        monMap.get(monitorID).secThreadList.put(nxtCrsList.get(k), st);
                    }else {
                        System.out.println("---"+ styleUrlID + " found in KML but status is not #clear");
                    }
                    //if(nl2.item(i).getLastChild().getNodeName())
                    //nl2.item(i).appendChild(TimeStamp);
                    //nl2.item(i).getLastChild().appendChild(when);
                    
                    //udpate in kml the section line to be occupied when there is a blockage signal
                    String lineName = monitorID + "-" + styleUrlID;
                    String lineName2 = styleUrlID + "-"+ monitorID;
                    for(int j = 0; j < len; j++){
                        e = (Element) nl.item(j);   
                        if(lineName.equals(e.getAttribute("id")) || lineName2.equals(e.getAttribute("id"))) {
                            e.setTextContent("#occupiedLine");
                        }
                    }
                }
            } 
        }
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new File("EmersonSensors.kml"));
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
    }
    
}
