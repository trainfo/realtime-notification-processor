/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realtimeNotificationProcessor;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chao at Morr Information Systems
 */
public class DBAgent {
    //set up db connection 
    ResultSet rs;
    PreparedStatement stmt;
    Connection con;
    MysqlDataSource ds;
    List<Integer> secThreshold;
    List<Integer> segCrossing;

    public DBAgent(ResultSet rs, PreparedStatement stmt, Connection con, MysqlDataSource ds) {
        this.rs = rs;
        this.stmt = stmt;
        this.con = con;
        this.ds = ds;
    }
    
    public List getSectionThreshold(String startMonitor, String endMonitor) throws SQLException {
        //initiate secTh list
        this.secThreshold = new ArrayList<>();
        
        //compose query and execute
        String query = "SELECT min_threshold, max_threshold FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, startMonitor);
        stmt.setString(2, endMonitor);
        rs = stmt.executeQuery();
        
        //retrive data from results set
        while (rs.next()) {
            secThreshold.add(rs.getInt("min_threshold"));
            secThreshold.add(rs.getInt("max_threshold"));
        }
        
        return secThreshold;
    }
    
    public List getSegmentCrossing (String monitor1, String monitor2) throws SQLException {
        this.segCrossing = new ArrayList<>();
        String query = "SELECT id FROM section WHERE monitor1_id = ? AND monitor2_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, monitor1);
        stmt.setString(2, monitor2);
        rs = stmt.executeQuery();
        
        while (rs.next()) {
            int secID = rs.getInt("id");
            String query2 = "SELECT agcrossing_id FROM section2agcrossing WHERE section_id = ?";
            stmt = con.prepareStatement(query2);
            stmt.setInt(1, secID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                this.segCrossing.add(rs.getInt("agcrossing_id"));
            } 
        }
        return this.segCrossing;
    }
    
    public List getSegmentThreshold(String monitor_id, int agcrossing_id) throws SQLException {
        
        String query = "SELECT min_threshold, max_threshold FROM segment WHERE monitor_id = ? AND agcrossing_id = ?";
        stmt = con.prepareStatement(query);
        stmt.setString(1, monitor_id);
        stmt.setInt(2, agcrossing_id);
        rs = stmt.executeQuery();
        List<Integer> segThreshold = new ArrayList<>();
        while (rs.next()) {
            segThreshold.add(rs.getInt("min_threshold"));
            segThreshold.add(rs.getInt("max_threshold"));
        } 
        
        return segThreshold;
    }
        
}
